<?
	//Для индексации
	if (isset($_GET['_escaped_fragment_']))
	{
		require_once('no-ajax/noAjax.php');
		$noAjax = new noAjax('Авелон Арт','pages/');

		$noAjax->readRoutes('routes.json');
		$noAjax->throwPage();
		return;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<meta name="fragment" content="!">

		<meta name="description" content="Авелон Арт - ремонт квартир, офисов, строительство и ремонт коттеджей в Новосибирске">
		<meta name="keywords" content="авелонарт, ремонт, строительство, квартиры, офисы, коттеджи, Новосибирск">
		<meta name='yandex-verification' content='719c4a61c40434ed' />

		<link rel="stylesheet" href="css/bootstrap.min.css" >
		<link rel="stylesheet" href="css/lightbox.css" >

		<!--[if (IE 8)|(IE 7)|(IE 6)]>
			<link rel="stylesheet" type="text/css" href="/css/no-less.css" />
		<![endif]-->


		<link rel="stylesheet/less" type="text/css" href="css/main.less" />

		<title>Авелон Арт</title>
	</head>

	<body>
		<div class="container">
			<div class="header">
				<a href="index.html" class="alink">
					<div class="logo">
					</div>
				</a>

				<div class="nav" tag="top">
					<ul class="menu">
						<a href="/#!/index.html" class="alink"><li class="active">Главная</li></a>
						<a href="/#!/about.html" class="alink"><li>О нас</li></a>
						<a href="/#!/portfolio/" class="alink"><li>Портфолио</li></a>
						<a href="/#!/prices.html" class="alink"><li>Цены</li></a>
						<a href="/#!/contacts.html" class="alink"><li>Контакты</li></a>
						<a href="/#!/feedback.html" class="alink"><li>Отзывы</li></a>
					</ul>
				</div>
			</div>

			<div class="clear-both"></div>

			<div class="content">
				<noscript><span>Для корректной работы сайта необходимо включить JavaScript.</span></noscript>
			</div>

			<div class="clear-both"></div>

			<div class="footer">
				<a href="http://vk.com/avelonart" target="_blank"><img style="margin-top: 15px;" src="/img/vk.png"></a>
				<p class="copyright">© ООО "Авелон-АРТ"</p>
				<p class="addr">
				Комсомольский проспект, 24 , офис 526<br>
				тел.:310-60-58<br>
				с 9:00 - 20:00, пн-пт<br>
				<script type="text/javascript">eval(unescape('%64%6f%63%75%6d%65%6e%74%2e%77%72%69%74%65%28%27%3c%61%20%68%72%65%66%3d%22%6d%61%69%6c%74%6f%3a%41%76%65%6c%6f%6e%2e%41%72%74%40%62%6b%2e%72%75%22%3e%41%76%65%6c%6f%6e%2e%41%72%74%40%62%6b%2e%72%75%3c%2f%61%3e%27%29%3b'))</script></p>
			</div>

		</div>	
		<script src="js/less.min.js"></script>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script src="js/bootstrap.min.js"></script> 
		<script src="js/lightbox.min.js"></script> 

		<script src="js/siteController.js"></script>
		<script src="js/main.js"></script> 
	</body>

</html>