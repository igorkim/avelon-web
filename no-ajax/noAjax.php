<?
	class noAjax {
		private $url;
		private $template;
		private $siteName;
		private $routes;
		private $homeDir;

		public $rootDir;
		public $domain;

		public function __construct($siteName, $dir)
		{	
			if ($_GET['_escaped_fragment_'] == '')
			{
				$this->url  	= strpos($_SERVER['REQUEST_URI'], '?')>0?
									trim(substr($_SERVER['REQUEST_URI'], 1, strpos($_SERVER['REQUEST_URI'], '?')-1)):
									$_SERVER['REQUEST_URI'];				
			}
			else
			{
				$this->url = trim($_GET['_escaped_fragment_']);
			}


			$this->url = $this->trimSlash($this->url);
			
			$this->siteName	= $siteName;
			$this->homeDir 	= $dir;
			
			$this->routes 	= array();
		}

		private function trimSlash($str)
		{
			while ($str[0]=='/')
				$str = substr($str, 1, strlen($str));
			while ($str[strlen($str)-1]=='/')
				$str = substr($str, 0, strlen($str)-1);
			return $str;
		}

		public function readRoutes($path)
		{
			$routes 		= file_get_contents($path);
			$this->routes 	= json_decode($routes, true);
		}

		public function addRoute($from, $to, $title='', $indexing=true)
		{
			$this->routes[$from]['url'] 		= $to;
			$this->routes[$from]['title'] 		= ($title=='')?$this->siteName:$this->siteName.' - '.$title;
			$this->routes[$from]['indexing'] 	= $indexing;
		}

		public function saveRoutes($path='routes.json')
		{
			file_put_contents($path, json_encode($this->routes));
		}

		public function createSiteMap()
		{
			$siteMap 	= '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			$siteMap 	.= '<urlset>'."\n";
			
			foreach ($this->routes as $page)
			{
				if (!$page['indexing']) continue;
				$siteMap 	.= "\t"."\t".'<url>'."\n";
				$siteMap 	.= "\t"."\t\t".'<loc>'.$this->domain.$page['url'].'</loc>'."\n";
				$siteMap 	.= "\t"."\t\t".'<changefreq>monthly</changefreq>'."\n";
				$siteMap 	.= "\t"."\t".'</url>'."\n";
			}
			$siteMap 	.= ' </urlset>'."\n";

			file_put_contents($this->rootDir.'sitemap.xml', $siteMap);

			$robots 	= 'User-agent: *'."\n";
			$robots 	.= 'Allow: /'."\n";
			$robots 	.= 'Sitemap: '.$this->domain.'sitemap.xml'."\n";
			file_put_contents($this->rootDir.'robots.txt', $robots);
		}

		public function throwPage()
		{
			$page 	= array_key_exists($this->url, $this->routes)?$this->routes[$this->url]['url']:$this->url;
			$title 	= array_key_exists($this->url, $this->routes)?$this->routes[$this->url]['title']:$this->siteName;
			
			if (file_exists($this->homeDir.$page))
			{
				echo '<title>'.$title.'</title>';
				echo '<meta name="description" content="Авелон Арт - ремонт квартир, офисов, строительство и ремонт коттеджей в Новосибирске">';
				echo '<meta name="keywords" content="авелонарт, ремонт, строительство, квартиры, офисы, коттеджи, Новосибирск">';
				echo '<meta charset="utf-8">';

				echo file_get_contents($this->homeDir.'navigation.html');
				echo file_get_contents($this->homeDir.$page);
			}
			else
			{
				header("HTTP/1.0 404 Not Found");
			}
		}
	}
?>