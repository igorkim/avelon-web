<?
	require_once('noAjax.php');
	$noAjax = new noAjax('Авелон Арт','pages/');

	$noAjax->rootDir = '../';
	$noAjax->domain = 'http://avelonart.ru/';


	$noAjax->addRoute('404', '404.html', 'Страница не найдена', false);
	$noAjax->addRoute('404.html', '404.html', 'Страница не найдена', false);

	$noAjax->addRoute('', 'index.html', 'Главная');
	$noAjax->addRoute('index', 'index.html', 'Главная');
	$noAjax->addRoute('index.html', 'index.html', 'Главная');

	$noAjax->addRoute('about', 'about.html', 'О компании');
	$noAjax->addRoute('about.html', 'about.html', 'О компании');

	$noAjax->addRoute('portfolio', 'portfolio.html', 'Портфолио');
	$noAjax->addRoute('portfolio.html', 'portfolio.html', 'Портфолио');

	$noAjax->addRoute('contacts', 'contacts.html', 'Контакты');
	$noAjax->addRoute('contacts.html', 'contacts.html', 'Контакты');

	$noAjax->addRoute('prices', 'prices.html', 'Цены');
	$noAjax->addRoute('prices.html', 'prices.html', 'Цены');

	$noAjax->addRoute('special', 'special.html', 'Специальное предложение');
	$noAjax->addRoute('special.html', 'special.html', 'Специальное предложение');
	$noAjax->saveRoutes('../routes.json');

	$noAjax->createSiteMap();

	echo "Routes updated";
?>