<?
	class AppController 
	{
		public $mailto = array('Avelon.Art@bk.ru');
		//public $mailto = array('igor.skh@gmail.com');
		private $sqlConnection;

		//Функция подключения к БД MySQL
		private function sqlConnect()
		{
			$con=mysqli_connect("localhost","avelon_main","gKvKLszF","avelon_main");
			// Check connection
			if (mysqli_connect_errno()) {
				echo "Не удалось подключиться к базе данных MySQL: " . mysqli_connect_error();
			}		
			return $this->sqlConnection = $con; //Возвращает указатель на соединение с MySQL в случае успешного выполнения, или FALSE при неудаче
		}

		public function __construct()
		{	
			$this->sqlConnect();
			if ($_SERVER['REQUEST_METHOD'] === 'POST') 
			{
				if (!isset($_POST['mode']))
				{
					echo "Неизвестный запрос";
					return 0;
				}

				if ($_POST['mode'] == 'request')
				{
					$sendArray['name'] 		= trim(htmlspecialchars($_POST['name']));
					$sendArray['phone'] 	= trim(htmlspecialchars($_POST['phone']));
					$sendArray['comment'] 	= nl2br(trim(htmlspecialchars($_POST['comment'])));
					$sendArray['title'] 	= 'Запрос обратного звонка';
					if ($this->sendRequest($sendArray))
					{
						echo "Ваш запрос отправлен";
						return 1;
					}
					else
					{
						echo "Серверная ошибка, запрос не отправлен";
						return 0;						
					}
				}
				elseif ($_POST['mode'] == 'feedback')
				{
					$sendArray['name'] 	= trim(htmlspecialchars($_POST['name']));
					$sendArray['text'] 	= nl2br(trim(htmlspecialchars($_POST['text'])));

					if ($this->addFeedback($sendArray))
					{
						echo "Ваш отзыв отправлен и будет отображен на сайте после проверки модератором";
						return 1;
					}
					else
					{
						echo "Серверная ошибка, отзыв не отправлен";
						return 0;						
					}		
				}
			}
			elseif (isset($_GET['feedback_hash']) && isset($_GET['feedback_action']))
			{
				$action = $_GET['feedback_action'];
				$hash 	= $_GET['feedback_hash'];

				if ($action == 'confirm')
				{
					if ($this->confirmFeedback($hash))
					{
						echo "Запрос одобрен";
					}
				}
				elseif ($action == 'disable')
				{
					if ($this->disableFeedback($hash))
					{
						echo "Запрос отклонен";
					}
				}
				elseif ($action == 'delete')
				{
					if ($this->deleteFeedback($hash))
					{
						echo "Запрос удален";
					}
				}
			}
			elseif (isset($_GET['feedbacks']))
			{
				echo $this->getFeebacks();
			}
			else
			{
				echo "Не получен запрос";
				return 0;
			}
		}

		private function sendRequest($array)
		{
			if (count($this->mailto)==1) $to = $this->mailto[0];
			elseif (count($this->mailto)>1) $to = implode(', ', $this->mailto);

			// тема письма
			$subject = 'avelonart.ru: '.$array['title'];

			// текст письма
			$message 	= 'Имя: '.$array['name']."<br/>";
			$message 	.= 'Телефон: '.$array['phone']."<br/>";
			$message 	.= 'Текст сообщения: <br/>'.$array['comment']."<br/>";
			if (isset($array['text']))
			{
				$message 	.= 'Сообщение:<br/>'.$array['text'];
			}

			// Для отправки HTML-письма должен быть установлен заголовок Content-type
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

			// Отправляем
			return mail($to, $subject, $message, $headers);
		}

		private function addFeedback($array)
		{
			$hash = md5($array['text'].date('dmYHis'));
			//Делаем запрос к базе на добавление
			$result 	= mysqli_query($this->sqlConnection,
											"INSERT INTO feedbacks (`id`, `name`, `text`, `hash`) 
											VALUES (NULL, '".$array['name']."', '".$array['text']."', '$hash')"
										);		
			//Если запрос прошел
			if ($result)
			{
				$sendArray['name'] 	= $array['name'];
				$sendArray['text'] 	= $array['text'];
				$sendArray['title'] = 'Новый отзыв';

				$sendArray['text'] .= '<br/><br/>';
				$sendArray['text'] .= '<a href="http://avelonart.ru/phpController.php?feedback_hash='.
										$hash.'&feedback_action=confirm">Одобрить</a>' . ' | ' .
										'<a href="http://avelonart.ru/phpController.php?feedback_hash='.
										$hash.'&feedback_action=delete">Удалить</a>';
				$this->sendRequest($sendArray);

				return 1;
			}
			else
			{
				echo "SQL error: ".mysqli_error($this->sqlConnection);
				return 0;
			}	
		}

		private function getFeebacks()
		{
			$query  = "SELECT id,name,text FROM feedbacks WHERE status = 1 ORDER BY timestamp DESC";

			//Проводим запрос
			$result = mysqli_query($this->sqlConnection,$query);

			//Инициализируем массив
			$this->feedbacks = array();
			if ($result)
			{
				//Перебираем строки
				while ($row = mysqli_fetch_array($result))
				{
					$element 			= array();
					$element['id'] 		= $row['id'];
					$element['name'] 	= $row['name'];
					$element['text'] 	= $row['text'];
					$this->feedbacks[] 	= $element;
				}				
			}

			return json_encode($this->feedbacks);
		}

		private function deleteFeedback($hash)
		{
			$hash 		= trim(strip_tags($hash));
			if (!empty($hash))
			{
				//Делаем запрос к базе на добавление
				$result 	= mysqli_query($this->sqlConnection,"DELETE FROM feedbacks WHERE hash = '$hash'");
				//Если все ок, возвращаем ID записи если нет - текст ошибки
				return $result;						
			}			
		}

		private function confirmFeedback($hash)
		{
			$hash 		= trim(strip_tags($hash));
			if (!empty($hash))
			{		
				//Выполняем запрос обновления
				$result = mysqli_query($this->sqlConnection,"UPDATE feedbacks SET status = '1' WHERE hash = '$hash'");
				//Возвращаем 1 или сообщение об ошибке если запрос не прошел
				return $result;						
			}				
		}

		private function disableFeedback($hash)
		{
			$hash 		= trim(strip_tags($hash));
			if (!empty($hash))
			{		
				//Выполняем запрос обновления
				$result = mysqli_query($this->sqlConnection,"UPDATE feedbacks SET status = '0' WHERE hash = '$hash'");
				//Возвращаем 1 или сообщение об ошибке если запрос не прошел
				return $result;						
			}			
		}
	}
?>