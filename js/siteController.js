/******************************
Разработчик/Developer: Igor Kim
E-mail: kim@roundeasy.ru
*******************************/
var siteController = function() 
{
	//Проверяем подключена ли библиотека jQuery, без нее не будет работать
	if(!window.jQuery)
	{
		document.write('<center><h2>Библиотека jQuery не подключена</h2></center>')
		return
	}

	//Селектор на область контента
	this.contentSelector 	= '.content'
	//Селектор для ajax ссылок
	this.linkSelector 		= '.alink'

	//Название заголовок сайта
	this.siteName 			= 'Авелон Арт'
	//Корневая директория
	this.rootDir 			= ''
	//Путь к файлу с путями, маршрутизацией
	this.routesPath 		= "/routes.json"
	//HTML-код для 404 страницы (если не удалось получить 404 странциу из таблицы маршрутизации)
	this.notFoundMessage 	= '<article><center>Страница не найдена</center></article>'
	//HTML-код при загрузке
	this.loadMessage 		= '<article><center>Загрузка</center></article>'

	//Обнуляем таблицу маршрутизации
	this.routes 			= new Array()

	//Для запуска и работы сайта необходимо запустить метод run класса
	//Не вызываем в конструкторе, потому что может понадобиться переопределение параметров
}

//Метод запуска сайта (используется один раз при инициализации)
siteController.prototype.run = function() 
{	
	//Получаем таблицу маршрутизации из JSON файла
	this.getRoutes()
	//Загружаем страницу, которая указана сейчас в адресной строке
	this.loadPage(this.getPage(), true)

	//Переопределяем this
	componentThis = this
	//Сообщение о загрузке выводим в область контента
	$( document ).ajaxStart(function() 
	{
		$(componentThis.contentSelector).html(componentThis.loadMessage)
	})
	//Привязываем к ссылкам AJAX событие на загрузку страницы
	$(document).on('click',componentThis.linkSelector,function(event) {
		//Не даем переходить по ссылке 
		event.preventDefault()
		//Вызываем метод загрузки страницы
		componentThis.loadPage($(this).attr('href'))
	})
	//Вешаем обработчик на событие смены адресной строки (чтобы работали кнопки назад и вперед)
	window.addEventListener('popstate', function(e)
	{
		//Вызываем метод загрузки страницы
		componentThis.loadPage(componentThis.getPage(), true)
	}, false)
}

//Метод получаем таблицу маршрутизации из JSON-файла
siteController.prototype.getRoutes = function() 
{
	//через AJAX получаем содержимое файла
	this.routes = $.ajax({
		type: "GET",
		url: this.routesPath,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
	}).responseText

	//Парсим JSON, получаем на выходе объект, ассоциативный массив
	this.routes = $.parseJSON(this.routes)
}

//Функция удаляет лишние слэши в начале и конце строки
siteController.prototype.parseURL = function(url) 
{
	while (url.indexOf('#!')>-1)
		url = url.substring(url.indexOf('#!')+2,url.length)
			
	//Пока последний символ строки слэш
	while (url[url.length-1]=='/')
		//Заменяем строку на ее же без последнего символа
		url = url.substring(0, url.length-1)

	//Пока первый символ строки слэш
	while (url[0]=='/')
		//Заменяем на ее же без первого символа
		url = url.substring(1,url.length)

	return url
}

//Получаем страницу, указанную в адресной строке браузера
siteController.prototype.getPage = function(url) 
{
	var
		//По умолчанию страница пустая
		pageUrl 	= '',
		//Получаем из адресной строки только путь без имени сервера
		pageName 	= (location.href.indexOf('#!')==-1)?$(location).attr('pathname'):location.hash

	while (pageName.indexOf('#!')>-1)
		pageName = pageName.substring(pageName.indexOf('#!')+2,pageName.length)

	//Разделяем через слэш
	pageArr = pageName.split('/')

	//Соединяем составляющие
	for (var i=0;i<pageArr.length;i++)
	{
		if (pageArr[i]!='')
		{
			pageUrl += pageArr[i]
			if (i!=pageArr.length-1)
				pageUrl += '/'
		}
	}
	//Удаляем лишние сэши, возвращаем имя страницы
	return this.parseURL(pageUrl)
}

//Метод отображает страницу в области контента this.contentSelector
siteController.prototype.parseInclude  = function() 
{
	//Переопределяем this
	componentThis = this	
	//Сделаем активной ссылку меню, проходим по всем ссылкам меню
	$.each( $('.include_element'), function() {
		includeName = $(this).attr('href')

		//Получаем контент
		$.get('/'+componentThis.rootDir+'pages/include/'+includeName+'.html') 
			.done(function(data) {
				$.each($('.include_element.'+includeName), function() {
					$(this).html(data)
				})
			})
	});	
}

//Метод отображает страницу в области контента this.contentSelector
siteController.prototype.loadPage  = function(pageUrl, popstate) 
{
	//Удаляем лишние слэши в пути к странице
	pageUrl = this.parseURL(pageUrl)

	var 
		//Оригинальная ссылка, до поиска в таблице маршрутизации
		originPage 	= pageUrl,
		//Заголовок по-умолчанию считаем название сайта
		pageTitle	= this.siteName

	//Если popstate неопределен ставим значение в false
	popstate = (typeof popstate !== 'undefined')?popstate:false

	//Если есть запись в таблице маршрутизации
	if(typeof this.routes[pageUrl] != 'undefined') 
	{
		//Получаем реальный путь к файлу из таблицы маршрутизации
		pageUrl 	= this.routes[pageUrl]['url']
		//Получаем заголовок страницы из таблицы маршрутизации
		pageTitle	= this.routes[pageUrl]['title']
	}

	//Переопределяем this
	componentThis = this
	//Получаем контент
	$.get('/'+this.rootDir+'pages/'+pageUrl) 
		.done(function(data) { 
			$(document).attr('title', pageTitle)
			$(componentThis.contentSelector).html(data)
			componentThis.parseInclude()
		}).fail(function() { 
			//Если загрузить файл не удалось, и эта страница была 404, то выводим стандартное сообщение в область контента
			if (originPage=='404')
				$(componentThis.contentSelector).html(componentThis.notFoundMessage)
			//Если страница которую пытались загрузить была не 404, пробуем загрузить 404
			else 
				componentThis.loadPage('404',true)
		})

	//Сделаем активной ссылку меню, проходим по всем ссылкам меню
	$.each( $('.menu li'), function() {
	  	$(this).removeClass('active')
	  	//Получаем URL ссылки
	  	compareUrl = componentThis.parseURL($(this).parent().attr('href'))

	  	//Массив из адреса без маршрутизации
		pageArrOrigin 	= originPage.split('/')
		//Массив из адреса после просмотра таблицы маршрутизации
		pageArr 		= pageUrl.split('/')

		//Проходим по всем элементам массивов пути
		for (var i=0;i<Math.max(pageArr.length,pageArrOrigin.length);i++)
		{
			originCompare 	= (typeof pageArrOrigin[i] !== 'undefined')?pageArrOrigin[i]:pageArr[i]
			routeCompare 	= (typeof pageArr[i] !== 'undefined')?pageArr[i]:pageArrOrigin[i]

			//При первом совпадении в пути
		  	if (compareUrl==originCompare || 
		  		compareUrl==routeCompare || 
		  		compareUrl=='/#!'+originCompare || 
		  		compareUrl=='/#!'+routeCompare) 
		  	{
		  		//Присваиваем статус активным
		  		$(this).addClass('active')
		  		//Прерываем цикл
		  		break
		  	}
		}
	});	

	//Определяем состояния для записи в историю бразуера относительно домена
	state = (originPage[0]!='/')?'/'+originPage:originPage
	//Если необходимо добавляем данную ссылку в историю браузера, для навигации кнопками вперед-назад
	if (!popstate) 
		window.history.pushState("", "", state)
}

siteController.prototype.sendRequest = function(formSelector) 
{
	name 	= $(formSelector).find('#requestFormName').val()
	phone 	= $(formSelector).find('#requestFormPhone').val()
	comment = $(formSelector).find('#requestTextarea').val()

	if (name=='' || phone=='')
	{
		alert('Пожалуйста, введите имя и номер телефона')
		return false
	}
	
	$.ajax({
		type: "POST",
		url: "/phpController.php",
		data: { name: name, phone: phone, comment: comment, mode: 'request' },
		global: false, 
	})
	.done(function( msg ) {
		$(formSelector).html(msg);
	});

	return false
}

siteController.prototype.sendFeedback = function(formSelector) 
{
	name 	= $(formSelector).find('#feedbackFormName').val()
	text 	= $(formSelector).find('#feedbackFormText').val()

	if (name=='' || text=='')
	{
		alert('Пожалуйста, заполните все поля')
		return false
	}
	
	$.ajax({
		type: "POST",
		url: "/phpController.php",
		data: { name: name, text: text, mode: 'feedback' },
		global: false, 
	})
	.done(function( msg ) {
		$(formSelector).html(msg);
	});

	return false
}

siteController.prototype.feedbacks = function(selector) 
{
	$.getJSON( "/phpController.php?feedbacks", function( data ) {
		var items = [];
		console.log(data)
		$.each( data, function( key, val ) {
			console.log(val.text)
			items.push( "<div class='feedback' id='" + val.id + "'>" 
							+ '<h2>'+val.name+'</h2>'
							+ '<p>'+val.text+'</p>' + 
							"</div>" );
		});

		$('.feedbacks').html('')
		
		$( "<div/>", {
			"class": "feedbacks-list",
			html: items.join( "" )
		}).appendTo( ".feedbacks" );
	});	

	return false
}